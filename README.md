This repository contains Python scripts as well as datasets for "Embedded omics data geometry untangles biological complexity" by Alexandros Xenos, Noël Malod-Dognin, Stevan Milinković and Nataša Pržulj.

**Corresponding author:** Prof. N. Przulj, **e-mail:** natasha@bsc.es 



**Instructions**

*  To compute the GDV similarities for the Homo sapiens network run the following command from the project directory: `python gdv_sim.py`
*  To generate the embedding of the Homo sapiens network first compute the GDV similarties of its nodes and then run the following command from the project directory: `python main_embeddings.py`  
   This command takes two arguements: the first is to define the type of matrix (ADJ, PPMI, GDV, GDV PPMI) and the second one is to define the factorization technique (SVD, NMTF).